#include "../OrbTransform/OrbTransform.h"

/***********************************************\
	Purpose	:	Just so the dll would load up correctly,
				maybe add some indication that the dll was loaded.

	Note	:	We do not invoke the main logic here because
				if it failed we will not get any indicitive error.

				This function will be invoked by LoadLibrary, which can
				only return TRUE or FALSE. 

				Because of this, the dllmain is empty, and the main
				logic will be at ORBTRANSFORM_ChangeStartOrb, which we will
				invoke using a little shellcode, start it with CreateRemoteThread,
				and than get the exit code. 
				This was we can know if everything went okay :)
\***********************************************/
BOOL WINAPI DllMain(
	HINSTANCE hinstDLL,
	DWORD fdwReason,
	LPVOID lpvReserved
) {
	UNREFERENCED_PARAMETER(hinstDLL);
	UNREFERENCED_PARAMETER(fdwReason);
	UNREFERENCED_PARAMETER(lpvReserved);

	return TRUE;
}