#pragma once
/***********************************************\
	Purpose	:	This module includes all the common
				header files and definitions

\***********************************************/

#pragma warning(push, 1)
#include <windows.h>
#pragma warning(pop)

#include "errors.h"

#define __IN
#define __OUT

#define ARRAY_SIZE(x) ((sizeof(x)) / sizeof(x[0]))


/*********************************************************\
	Arguments	:	dll_path	- The path to the dll to load, as wide char
					module		- An out parameter, will accept the module
								  if lodaded correctly

	Purpose		:	Loads the specified module to memory

	Notes		:	None
\*********************************************************/
ORB_ERROR ORBTRANSFORM_LoadLibraryW(
	__IN	LPWSTR	dll_path,
	__OUT	HMODULE	*module
);

/*********************************************************\
	Arguments	:	module	- Library to unload, will be set to NULL
							  after unloading

	Purpose		:	Loads the specified module to memory

	Notes		:	None
\*********************************************************/
ORB_ERROR ORBTRANSFORM_FreeLibrary(
	__IN __OUT	HMODULE	*module
);

/*********************************************************\
	Arguments	:	module			- The module to get the proc from
					name_of_ordinal	- The name or ordinal of the function 
					out_func		- Out parameter for the function

	Purpose		:	Finds the proc address in the specified module

	Notes		:	None
\*********************************************************/
ORB_ERROR ORBTRANSFORM_GetProcAddress(
	__IN	HMODULE module,
	__IN	LPCSTR	name_or_ordinal,
	__OUT	FARPROC *out_func
);