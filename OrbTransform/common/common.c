#include "common.h"

ORB_ERROR ORBTRANSFORM_LoadLibraryW(
	__IN	LPWSTR	dll_path,
	__OUT	HMODULE	*out_module
) {
	ORB_ERROR	error	= ORB_UNINITIALIZED;
	HMODULE		module	= NULL;

	if (out_module == NULL) {
		error = ORB_INVALID_POINTER;
		goto lblCleanup;
	}

	module = LoadLibraryW(dll_path);
	if (module == NULL) {
		error = ORB_LOAD_LIBRARY_FAILED;
		goto lblCleanup;
	}

	// SUCCESS!
	error = ORB_SUCCESS;
	*out_module = module;
lblCleanup:
	return error;
}

ORB_ERROR ORBTRANSFORM_FreeLibrary(__IN __OUT HMODULE * module)
{
	ORB_ERROR	error = ORB_UNINITIALIZED;

	if (module == NULL) {
		error = ORB_INVALID_POINTER;
		goto lblCleanup;
	}

	FreeLibrary(*module);

	// SUCCESS!
	error	= ORB_SUCCESS;
	*module = NULL;
lblCleanup:
	return error;
}

ORB_ERROR ORBTRANSFORM_GetProcAddress(
	__IN	HMODULE module,
	__IN	LPCSTR	name_or_ordinal,
	__OUT	FARPROC *out_func
) {
	ORB_ERROR	error	= ORB_UNINITIALIZED;
	FARPROC		proc	= NULL;

	if (out_func == NULL) {
		error = ORB_INVALID_POINTER;
		goto lblCleanup;
	}

	proc = GetProcAddress(module, name_or_ordinal);
	if (proc == NULL) {
		error = ORB_LOAD_LIBRARY_FAILED;
		goto lblCleanup;
	}

	// SUCCESS!
	error = ORB_SUCCESS;
	*out_func = proc;
lblCleanup:
	return error;
}