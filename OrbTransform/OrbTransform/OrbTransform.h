#pragma once

#include "../common/common.h"

#define CLASSIC_SHELL_IMPORT_SETTINGS_SUCCSES	(0)
#define CLASSIC_SHELL_IMPORT_SETTINGS_FAILURE	(1)

#define ORBTRANSFORM_DESIRED_PROCESS	(L"explorer.exe")
#define ORBTRANSFORM_DESIRED_PATH		(L"C:\\Windows\\explorer.exe")
#define ORBTRANSFORM_CLASSIC_SHELL		(L"ClassicShellMenuDLL.dll")
#define ORBTRANSFORM_SETTINGS_XML		(L"C:\\temp\\classic_settings.xml")

#define IMPORT_SETTINGS_XML				("?DllImportSettingsXml@@YA_NPEB_W@Z")

typedef BOOL (__cdecl *PFN_ImportSettingsFromXML)(wchar_t const * __ptr64);

/*********************************************************\
	Arguments	:	None

	Purpose		:	This function transforms the windows start orb
					to the image we want

	Notes		:	None
\*********************************************************/
ORB_ERROR ORBTRANSFORM_ChangeStartOrb(void);

/*********************************************************\
	Arguments	:	None

	Purpose		:	Verifies that we are loaded into the process we 
					are suppose to be loaded to. Specifically, explorer.exe.
					This will check the module name and compare it to the
					executable and the entire path.
\*********************************************************/
static ORB_ERROR ORBTRANSFORM_VerifyProcess(void);

/*********************************************************\
	Arguments	:	class_shell		-	Out parameter for the variable that will
										hold the classic shell module
					import_settings -	Out parameter for the variable that will
										hold the function pointer to the import
										settings function in the module

	Purpose		:	Load the classic shell module and import settings function
					into the appropriate arguments

	Notes		:	None
\*********************************************************/
static ORB_ERROR ORBTRANSFORM_LoadClassicShellAndOrdinal(
	__OUT HMODULE *classic_shell,
	__OUT FARPROC *import_settings
);

/*********************************************************\
	Arguments	:	import_settings -	Function pointer to the import
										settings function in classic shell

	Purpose		:	Invokes the import settings function, which in turn
					will hopefully change the start orb :)

	Notes		:	None
\*********************************************************/
static ORB_ERROR ORBTRANSFORM_InvokeImportSettings(
	__IN FARPROC *import_settings
);