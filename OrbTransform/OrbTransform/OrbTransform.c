#include "OrbTransform.h"

/***********************************************\
	Steps	:	1. Verify we are loaded to explorer.exe
				2. Load "ClassicStartMenuDLL.dll"
				3. Invoke entry point 5 (ImportSettingFromXML) passing
					the path to our crafted settings xml
				4. Verify success, report failures
				5. Unload "ClassicStartMeduDLL.dll"
\***********************************************/
ORB_ERROR ORBTRANSFORM_ChangeStartOrb(void)
{
	ORB_ERROR	error			= ORB_UNINITIALIZED;
	HMODULE		classic_shell	= NULL;
	FARPROC		import_settings = NULL;

	// Step 1 
	error = ORBTRANSFORM_VerifyProcess();
	ORB_CHECK(error);
	
	// Step 2
	error = ORBTRANSFORM_LoadClassicShellAndOrdinal(&classic_shell, &import_settings);
	ORB_CHECK(error);

	// Step 3 + 4
	error = ORBTRANSFORM_InvokeImportSettings(&import_settings);
	ORB_CHECK(error);

	// Step 5
	error = ORBTRANSFORM_FreeLibrary(&classic_shell);
	ORB_CHECK(error);
	import_settings = NULL;

	error = ORB_SUCCESS;
lblCleanup:
	return error;
}

static ORB_ERROR ORBTRANSFORM_VerifyProcess(void) {
	ORB_ERROR	error			= ORB_UNINITIALIZED;
	DWORD		result			= 0;
	wchar_t		exe_name[1024]	= { 0 };

	result = GetModuleFileNameW(NULL,
								exe_name,
								ARRAY_SIZE(exe_name));
	if (result == 0) {
		error = ORB_GET_MODULE_FILE_NAME_FAILED;
		goto lblCleanup;
	}
	if ((wcscmp(exe_name, ORBTRANSFORM_DESIRED_PROCESS) != 0) &&
		(wcscmp(exe_name, ORBTRANSFORM_DESIRED_PATH)	!= 0)) 
	{
		error = ORB_WRONG_PROCESS;
		goto lblCleanup;
	}

	// Verified process! return success
	error = ORB_SUCCESS;
lblCleanup:
	return error;
}

static ORB_ERROR ORBTRANSFORM_LoadClassicShellAndOrdinal(
	__OUT HMODULE *classic_shell,
	__OUT FARPROC *import_settings
) {
	ORB_ERROR	error	= ORB_UNINITIALIZED;
	HMODULE		module	= NULL;
	FARPROC		func	= NULL;

	error = ORBTRANSFORM_LoadLibraryW(ORBTRANSFORM_CLASSIC_SHELL, &module);
	ORB_CHECK(error);

	error = ORBTRANSFORM_GetProcAddress(module, IMPORT_SETTINGS_XML, &func);
	ORB_CHECK(error);

	// SUCCESS!!
	*classic_shell		= module;
	*import_settings	= func;
	error				= ORB_SUCCESS;
lblCleanup:
	return error;
}

ORB_ERROR ORBTRANSFORM_InvokeImportSettings(
	__IN FARPROC * import_settings
) {
	ORB_ERROR	error					= ORB_UNINITIALIZED;
	HRESULT		result					= -1;
	BOOL		import_settings_result	= FALSE;

	result = CoInitialize(NULL);
	if ((result != S_OK) && (result != S_FALSE)) {
		error = ORB_COINITIALZE_FAILED;
		goto lblCleanup;
	}

	import_settings_result = ((PFN_ImportSettingsFromXML)(*import_settings))(
		ORBTRANSFORM_SETTINGS_XML
	);
	if (import_settings_result != CLASSIC_SHELL_IMPORT_SETTINGS_SUCCSES) {
		error = ORB_IMPORT_SETTINGS_XML_FAILED;
		goto lblCleanup;
	}	

	error = ORB_SUCCESS;
lblCleanup:
	CoUninitialize();
	return error;
}